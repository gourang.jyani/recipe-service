package com.abn.recipe.recipeservice.service;

import com.abn.recipe.recipeservice.dao.RecipeRepository;
import com.abn.recipe.recipeservice.dto.PagedResultResponseDto;
import com.abn.recipe.recipeservice.dto.RecipeDto;
import com.abn.recipe.recipeservice.entity.Recipe;
import com.abn.recipe.recipeservice.exception.ValidationException;
import com.abn.recipe.recipeservice.transformer.RecipeTransformer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class RecipeServiceImplTest extends AbstractEntityServiceTest {

    @Mock
    private RecipeRepository repository;
    @Mock
    private RecipeTransformer transformer;

    @InjectMocks
    private RecipeServiceImpl unit;

    @Test
    public void testAddHappyFlow() {
        RecipeDto newRecipeDto = getRecipeDto(null, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        Recipe newRecipe = getRecipe(null, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        Recipe newRecipeWithId = getRecipe(RECIPE_ID_1001, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");

        Mockito.when(transformer.transformFromDto(newRecipeDto)).thenReturn(newRecipe);
        Mockito.when(repository.save(newRecipe)).thenReturn(newRecipeWithId);
        boolean response = unit.add(newRecipeDto);
        Assertions.assertTrue(response);

    }


    @Test
    public void testAddInvalidWithoutName() {
        RecipeDto newRecipeDto = getRecipeDto(null, "", Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        Assertions.assertThrows(ValidationException.class, () ->
                {
                    unit.add(newRecipeDto);
                }
        );
    }

    @Test
    public void testAddInvalidWithInvalidServings() {
        RecipeDto newRecipeDto = getRecipeDto(null, RECIPE_NAME_1002, Boolean.TRUE, -1L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        Assertions.assertThrows(ValidationException.class, () ->
                {
                    unit.add(newRecipeDto);
                }
        );
    }

    @Test
    public void testAddInvalidWithEmptyIngredients() {
        RecipeDto newRecipeDto = getRecipeDto(null, RECIPE_NAME_1002, Boolean.TRUE, 2L, Collections.emptySet(), "Instructions for cooking");
        Assertions.assertThrows(ValidationException.class, () ->
                {
                    unit.add(newRecipeDto);
                }
        );
    }

    @Test
    public void testAddInvalidWithEmptyInstructions() {
        RecipeDto newRecipeDto = getRecipeDto(null, RECIPE_NAME_1002, Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "");
        Assertions.assertThrows(ValidationException.class, () ->
                {
                    unit.add(newRecipeDto);
                }
        );
    }

    @Test
    public void testUpdateHappyFlow() {
        RecipeDto recipeDtoToUpdate = getRecipeDto(RECIPE_ID_1001, "NEW_NAME", Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        Recipe recipeToUpdate = getRecipe(RECIPE_ID_1001, "NEW_NAME", Boolean.TRUE, 2L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");

        Recipe recipeFromDB = getRecipe(RECIPE_ID_1001, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");

        Mockito.when(transformer.transformFromDto(recipeDtoToUpdate)).thenReturn(recipeToUpdate);
        Mockito.when(repository.findById(RECIPE_ID_1001)).thenReturn(java.util.Optional.ofNullable(recipeFromDB));

        Mockito.when(repository.save(recipeToUpdate)).thenReturn(recipeToUpdate);
        boolean response = unit.update(recipeDtoToUpdate);
        Assertions.assertTrue(response);
    }

    @Test
    public void testUpdateRecipeNotFound() {
        RecipeDto recipeDtoToUpdate = getRecipeDto(RECIPE_ID_1001, "NEW_NAME", Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        Recipe recipeToUpdate = getRecipe(RECIPE_ID_1001, "NEW_NAME", Boolean.TRUE, 2L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");

        Mockito.when(repository.findById(RECIPE_ID_1001)).thenReturn(java.util.Optional.empty());
        Assertions.assertThrows(ValidationException.class, () -> unit.update(recipeDtoToUpdate)
        );
    }

    @Test
    public void testDeleteHappyFlow() {
        Mockito.doNothing().when(repository).deleteById(RECIPE_ID_1001);
        boolean delete = unit.delete(RECIPE_ID_1001);

        Assertions.assertTrue(delete);
    }

    @Test
    public void testDeleteWithNullId() {
        Assertions.assertThrows(ValidationException.class, () -> unit.delete(null));
    }

    @Test
    public void testGetById() {
        Recipe recipeFromDB = getRecipe(RECIPE_ID_1001, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        RecipeDto recipeDto = getRecipeDto(RECIPE_ID_1001, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");

        Mockito.when(repository.findById(RECIPE_ID_1001)).thenReturn(Optional.ofNullable(recipeFromDB));
        Mockito.when(transformer.transformToDto(recipeFromDB)).thenReturn(recipeDto);

        unit.find(RECIPE_ID_1001);
    }

    @Test
    public void testGetByIdInvalid() {

        Mockito.when(repository.findById(RECIPE_ID_1001)).thenReturn(Optional.empty());
        RecipeDto recipeDto = unit.find(RECIPE_ID_1001);
        Assertions.assertNull(recipeDto);
    }

    @Test
    public void testFindAll() {
        Recipe recipeFromDB1 = getRecipe(RECIPE_ID_1001, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        Recipe recipeFromDB2 = getRecipe(RECIPE_ID_1002, RECIPE_NAME_1002, Boolean.FALSE, 3L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking R2");

        RecipeDto recipeDto1 = getRecipeDto(RECIPE_ID_1001, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");
        RecipeDto recipeDto2 = getRecipeDto(RECIPE_ID_1002, RECIPE_NAME_1002, Boolean.FALSE, 3L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking R2");

        PageImpl<Recipe> recipePage = new PageImpl<>(List.of(recipeFromDB1, recipeFromDB2), PageRequest.of(0, 2), 1);
        Mockito.when(repository.findAll(PageRequest.of(0, 10))).thenReturn(recipePage);
        Mockito.when(transformer.transformToDto(recipeFromDB1)).thenReturn(recipeDto1);
        Mockito.when(transformer.transformToDto(recipeFromDB2)).thenReturn(recipeDto2);

        PagedResultResponseDto<RecipeDto> recipesPagedResponseDto = unit.findAll(PageRequest.of(0, 10));
        Assertions.assertNotNull(recipesPagedResponseDto);
        Assertions.assertNotNull(recipesPagedResponseDto.getRecords());
        Assertions.assertEquals(2L, recipesPagedResponseDto.getRecords().size());
        Assertions.assertEquals(2, recipesPagedResponseDto.getNoOfRecords());
        Assertions.assertEquals(1, recipesPagedResponseDto.getCurrentPage());
        Assertions.assertEquals(1, recipesPagedResponseDto.getTotalPages());
        
    }


}
