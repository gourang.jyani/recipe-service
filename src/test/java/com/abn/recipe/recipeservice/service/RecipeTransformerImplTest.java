package com.abn.recipe.recipeservice.service;

import com.abn.recipe.recipeservice.dto.RecipeDto;
import com.abn.recipe.recipeservice.entity.Recipe;
import com.abn.recipe.recipeservice.transformer.RecipeTransformer;
import com.abn.recipe.recipeservice.transformer.RecipeTransformerImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class RecipeTransformerImplTest extends AbstractEntityServiceTest {

    private RecipeTransformer unit = new RecipeTransformerImpl();

    @Test
    public void testTransformToDto() {
        Recipe recipeFromDB1 = getRecipe(RECIPE_ID_1001, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredients(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredients(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");

        RecipeDto recipeDtoTransformed = unit.transformToDto(recipeFromDB1);

        Assertions.assertEquals(RECIPE_ID_1001, recipeDtoTransformed.getId());
        Assertions.assertEquals(RECIPE_NAME_1001, recipeDtoTransformed.getName());
        Assertions.assertEquals(2L, recipeDtoTransformed.getServingQuantity());
        Assertions.assertEquals(Boolean.TRUE, recipeDtoTransformed.isVegetarian());
        Assertions.assertEquals(recipeFromDB1.getInstructions(), recipeDtoTransformed.getInstructions());
        Assertions.assertEquals(recipeFromDB1.getIngredients().size(), recipeDtoTransformed.getIngredients().size());
    }

    @Test
    public void testTransformFromDto() {
        RecipeDto source = getRecipeDto(RECIPE_ID_1001, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");

        Recipe destination = unit.transformFromDto(source);

        Assertions.assertEquals(RECIPE_ID_1001, destination.getId());
        Assertions.assertEquals(RECIPE_NAME_1001, destination.getName());
        Assertions.assertEquals(2L, destination.getServingQuantity());
        Assertions.assertEquals(Boolean.TRUE, destination.isVegetarian());
        Assertions.assertEquals(source.getInstructions(), destination.getInstructions());
        Assertions.assertEquals(source.getIngredients().size(), destination.getIngredients().size());
    }

}
