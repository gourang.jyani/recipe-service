package com.abn.recipe.recipeservice.integration;

import com.abn.recipe.recipeservice.dto.RecipeDto;
import com.abn.recipe.recipeservice.dto.RecipesPagedResponseDto;
import com.abn.recipe.recipeservice.dto.ServiceResponseDto;
import com.abn.recipe.recipeservice.service.AbstractEntityServiceTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Set;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class RecipeServiceIntegrationTest extends AbstractEntityServiceTest {

    private static final String SERVER_ADDR = "http://localhost:";
    private static final String BASE_URI = "/recipe-service";
    private static final String API_RECIPE_ENDPOINT = "/api/recipe";
    private static final String SEARCH_URI = "/search";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void addRecipe() {
        String url = getEndpointURL(API_RECIPE_ENDPOINT);
        RecipeDto incomingRecipeDto = getRecipeDto(null, RECIPE_NAME_1001, Boolean.TRUE, 2L, Set.of(getIngredientsDto(INGREDIENT_ID_1001, INGREDIENT_NAME_1001), getIngredientsDto(INGREDIENT_ID_1002, INGREDIENT_NAME_1002)), "Instructions for cooking");

        ResponseEntity<ServiceResponseDto<Boolean>> responseEntity = this.restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(incomingRecipeDto), new ParameterizedTypeReference<>() {
        });

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());

        Assertions.assertTrue(responseEntity.getBody().getDto());
    }

    @Test
    public void testUpdateRecipe() {
        String url = getEndpointURL(API_RECIPE_ENDPOINT);

        RecipeDto recipeToBeUpdated = getRecipeDto(RECIPE_ID_6003, "Updated Recipe", Boolean.TRUE, 3L, Set.of(getIngredientsDto(INGREDIENT_ID_5001, INGREDIENT_NAME_1002)), "Updated Instructions for cooking");

        ResponseEntity<ServiceResponseDto<Boolean>> updatedResponseEntity = this.restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(recipeToBeUpdated), new ParameterizedTypeReference<>() {
        });

        Assertions.assertNotNull(updatedResponseEntity);
        Assertions.assertEquals(HttpStatus.OK, updatedResponseEntity.getStatusCode());
        Assertions.assertNotNull(updatedResponseEntity.getBody());

        Assertions.assertTrue(updatedResponseEntity.getBody().getDto());
    }

    @Test
    public void testFindRecipeById() {
        String url = getEndpointURL(API_RECIPE_ENDPOINT + "/" + RECIPE_ID_6001);

        ResponseEntity<ServiceResponseDto<RecipeDto>> responseEntity = this.restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
        });

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());

        Assertions.assertNotNull(responseEntity.getBody().getDto());
        Assertions.assertEquals(RECIPE_ID_6001, responseEntity.getBody().getDto().getId());
        Assertions.assertEquals("Fried Rice", responseEntity.getBody().getDto().getName());
        Assertions.assertEquals(Boolean.TRUE, responseEntity.getBody().getDto().isVegetarian());
        Assertions.assertEquals(2L, responseEntity.getBody().getDto().getServingQuantity());
        Assertions.assertEquals("Boil Rice, Add Onion, Chilli, Cook for 10 min", responseEntity.getBody().getDto().getInstructions());
        Assertions.assertEquals(3L, responseEntity.getBody().getDto().getIngredients().size());
    }

    @Test
    public void testDeleteRecipeById() {
        String url = getEndpointURL(API_RECIPE_ENDPOINT + "/" + RECIPE_ID_6003);

        ResponseEntity<ServiceResponseDto<Boolean>> responseEntity = this.restTemplate.exchange(url, HttpMethod.DELETE, null, new ParameterizedTypeReference<>() {
        });

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());
        Assertions.assertTrue(responseEntity.getBody().getDto());
    }

    @Test
    public void testFindAllRecipes() {
        String url = getEndpointURL(API_RECIPE_ENDPOINT);

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("page", 1)
                .queryParam("size", 10)
                .build();

        ResponseEntity<ServiceResponseDto<RecipesPagedResponseDto>> responseEntity = this.restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<>() {
        });

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());

        Assertions.assertNotNull(responseEntity.getBody().getDto());
        Assertions.assertNotNull(responseEntity.getBody().getDto().getRecords());
        Assertions.assertEquals(6, responseEntity.getBody().getDto().getRecords().size());
        Assertions.assertEquals(6, responseEntity.getBody().getDto().getNoOfRecords());

        Assertions.assertEquals(1, responseEntity.getBody().getDto().getCurrentPage());
        Assertions.assertEquals(1, responseEntity.getBody().getDto().getTotalPages());

    }

    private String getEndpointURL(String apiEndpoint) {

        return new StringBuilder(SERVER_ADDR)
                .append(port)
                .append(BASE_URI)
                .append(apiEndpoint)
                .toString();
    }
}
