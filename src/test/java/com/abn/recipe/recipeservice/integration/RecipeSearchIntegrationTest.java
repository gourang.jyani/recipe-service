package com.abn.recipe.recipeservice.integration;

import com.abn.recipe.recipeservice.dto.PageRequestDto;
import com.abn.recipe.recipeservice.dto.RecipeSearchRequestDto;
import com.abn.recipe.recipeservice.dto.RecipesPagedResponseDto;
import com.abn.recipe.recipeservice.dto.ServiceResponseDto;
import com.abn.recipe.recipeservice.service.AbstractEntityServiceTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URISyntaxException;
import java.util.Set;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class RecipeSearchIntegrationTest extends AbstractEntityServiceTest {

    private static final String SERVER_ADDR = "http://localhost:";
    private static final String BASE_URI = "/recipe-service";
    private static final String API_RECIPE_ENDPOINT = "/api/recipe";
    private static final String SEARCH_URI = "/search";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void searchRecipesByNameLike() throws URISyntaxException {
        String url = getEndpointURL(API_RECIPE_ENDPOINT + SEARCH_URI);

        RecipeSearchRequestDto searchRequest = RecipeSearchRequestDto.builder()
                .name("rice")
                .pageRequest(new PageRequestDto(0, 10))
                .build();

        ResponseEntity<ServiceResponseDto<RecipesPagedResponseDto>> responseEntity = getSearchResponseEntity(url, searchRequest);

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());

        Assertions.assertNotNull(responseEntity.getBody().getDto());
        Assertions.assertNotNull(responseEntity.getBody().getDto().getRecords());
        Assertions.assertEquals(3, responseEntity.getBody().getDto().getRecords().size());
        Assertions.assertEquals(3, responseEntity.getBody().getDto().getNoOfRecords());

        Assertions.assertEquals(1, responseEntity.getBody().getDto().getCurrentPage());
        Assertions.assertEquals(1, responseEntity.getBody().getDto().getTotalPages());
    }

    @Test
    public void searchRecipesByVegetarianFalse() throws URISyntaxException {
        String url = getEndpointURL(API_RECIPE_ENDPOINT + SEARCH_URI);

        RecipeSearchRequestDto searchRequest = RecipeSearchRequestDto.builder()
                .vegetarian(Boolean.FALSE)
                .pageRequest(new PageRequestDto(0, 10))
                .build();

        ResponseEntity<ServiceResponseDto<RecipesPagedResponseDto>> responseEntity = getSearchResponseEntity(url, searchRequest);

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());

        Assertions.assertNotNull(responseEntity.getBody().getDto());
        Assertions.assertNotNull(responseEntity.getBody().getDto().getRecords());
        Assertions.assertEquals(3, responseEntity.getBody().getDto().getRecords().size());
        Assertions.assertEquals(3, responseEntity.getBody().getDto().getNoOfRecords());

        Assertions.assertEquals(1, responseEntity.getBody().getDto().getCurrentPage());
        Assertions.assertEquals(1, responseEntity.getBody().getDto().getTotalPages());

    }

    @Test
    public void searchRecipesByVegetarianAndIngredientsAndServings() throws URISyntaxException {
        String url = getEndpointURL(API_RECIPE_ENDPOINT + SEARCH_URI);

        RecipeSearchRequestDto searchRequest = RecipeSearchRequestDto.builder()
                .vegetarian(Boolean.TRUE)
                .ingredientsIn(Set.of("rice"))
                .servingQuantity(2L)
                .pageRequest(new PageRequestDto(0, 10))
                .build();

        ResponseEntity<ServiceResponseDto<RecipesPagedResponseDto>> responseEntity = getSearchResponseEntity(url, searchRequest);

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());

        Assertions.assertNotNull(responseEntity.getBody().getDto());
        Assertions.assertNotNull(responseEntity.getBody().getDto().getRecords());
        Assertions.assertEquals(2, responseEntity.getBody().getDto().getRecords().size());
        Assertions.assertEquals(2, responseEntity.getBody().getDto().getNoOfRecords());

        Assertions.assertEquals(1, responseEntity.getBody().getDto().getCurrentPage());
        Assertions.assertEquals(1, responseEntity.getBody().getDto().getTotalPages());

    }

    @Test
    public void searchRecipesByIngredientsAndInstructions() throws URISyntaxException {
        String url = getEndpointURL(API_RECIPE_ENDPOINT + SEARCH_URI);

        RecipeSearchRequestDto searchRequest = RecipeSearchRequestDto.builder()
                .vegetarian(Boolean.TRUE)
                .ingredientsIn(Set.of("rice"))
                .instructionKeywords(Set.of("10 min"))
                .pageRequest(new PageRequestDto(0, 10))
                .build();

        ResponseEntity<ServiceResponseDto<RecipesPagedResponseDto>> responseEntity = getSearchResponseEntity(url, searchRequest);

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());

        Assertions.assertNotNull(responseEntity.getBody().getDto());
        Assertions.assertNotNull(responseEntity.getBody().getDto().getRecords());
        Assertions.assertEquals(2, responseEntity.getBody().getDto().getRecords().size());
        Assertions.assertEquals(2, responseEntity.getBody().getDto().getNoOfRecords());

        Assertions.assertEquals(1, responseEntity.getBody().getDto().getCurrentPage());
        Assertions.assertEquals(1, responseEntity.getBody().getDto().getTotalPages());

    }


    private ResponseEntity<ServiceResponseDto<RecipesPagedResponseDto>> getSearchResponseEntity(String url, RecipeSearchRequestDto searchRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url).build();

        return this.restTemplate.exchange(uriComponents.toUri(), HttpMethod.POST, new HttpEntity<>(searchRequest, headers), new ParameterizedTypeReference<>() {
        });
    }


    private String getEndpointURL(String apiEndpoint) {

        return new StringBuilder(SERVER_ADDR)
                .append(port)
                .append(BASE_URI)
                .append(apiEndpoint)
                .toString();
    }
}
