package com.abn.recipe.recipeservice.controller;

import com.abn.recipe.recipeservice.dto.RecipeDto;
import com.abn.recipe.recipeservice.dto.RecipeSearchRequestDto;
import com.abn.recipe.recipeservice.dto.RecipesPagedResponseDto;
import com.abn.recipe.recipeservice.dto.ServiceResponseDto;
import com.abn.recipe.recipeservice.exception.ErrorType;
import com.abn.recipe.recipeservice.exception.ValidationException;
import com.abn.recipe.recipeservice.service.RecipeService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/api/recipe")
@OpenAPIDefinition(info = @Info(title = "Recipe Management API", version = "1.0", description = "API to support various Recipe Operations"))
public class RecipeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecipeController.class);

    @Value("${recipe.search.default-page-size}")
    private int DEFAULT_PAGE_SIZE;

    private RecipeService recipeService;

    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    /**
     * API Endpoint to add new Recipe
     *
     * @param recipeDto
     * @return
     */
    @Operation(description = "API Endpoint to add new Recipe")
    @PostMapping
    public ServiceResponseDto<Boolean> add(@RequestBody RecipeDto recipeDto) {
        LOGGER.info("Processing request to a new recipe, request : {}", recipeDto);
        return new ServiceResponseDto<>(recipeService.add(recipeDto));
    }

    /**
     * API Endpoint to update existing Recipe
     *
     * @param recipeDto
     * @return
     */
    @Operation(description = "API Endpoint to update an existing Recipe")
    @PutMapping
    public ServiceResponseDto<Boolean> update(@RequestBody RecipeDto recipeDto) {
        LOGGER.info("Processing request to a update existing recipe, request : {}", recipeDto);
        return new ServiceResponseDto<>(recipeService.update(recipeDto));
    }

    /**
     * API Endpoint to delete a Recipe by Id
     *
     * @param recipeId
     * @return boolean
     */
    @Operation(description = "API Endpoint to add update Recipe")
    @DeleteMapping("/{recipeId}")
    public ServiceResponseDto<Boolean> delete(@PathVariable("recipeId") Long recipeId) {
        LOGGER.info("Processing request to a delete existing recipe, recipeId : {}", recipeId);
        return new ServiceResponseDto<>(recipeService.delete(recipeId));
    }

    /***
     *  API Endpoint to add fetch Recipe by Id
     * @param recipeId
     * @return boolean
     */
    @Operation(description = "API Endpoint to add fetch Recipe by Id")
    @GetMapping("/{recipeId}")
    public ServiceResponseDto<RecipeDto> fineOne(@PathVariable("recipeId") Long recipeId) {
        LOGGER.info("Processing request to a update existing recipe, request : {}", recipeId);
        return new ServiceResponseDto<>(recipeService.find(recipeId));
    }

    /**
     * API Endpoint to add fetch all Recipes
     *
     * @return
     */
    @Operation(description = "API Endpoint to add fetch all Recipes")
    @GetMapping
    public ServiceResponseDto<RecipesPagedResponseDto> findAll(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                               @RequestParam(value = "size", required = false) Integer pageSize) {
        LOGGER.info("Processing request to find all recipe");
        if (page < 1) {
            throw new ValidationException(ErrorType.INVALID_PAGE_REQUEST);
        }
        int pageIndex = page - 1;
        pageSize = (Objects.isNull(pageSize) || pageSize <= 0) ? DEFAULT_PAGE_SIZE : pageSize;

        return new ServiceResponseDto<>(recipeService.findAll(PageRequest.of(pageIndex, pageSize, Sort.by("name"))));
    }

    @Operation(description = "API Endpoint to implement search functionality for Recipes")
    @PostMapping("/search")
    public ServiceResponseDto<RecipesPagedResponseDto> searchRecipe(@RequestBody RecipeSearchRequestDto searchRequest) {
        LOGGER.info("Processing request to find all recipe");
        return new ServiceResponseDto<>(recipeService.search(searchRequest));
    }

}
