package com.abn.recipe.recipeservice.controller;

import com.abn.recipe.recipeservice.dto.IngredientsDto;
import com.abn.recipe.recipeservice.dto.ServiceResponseDto;
import com.abn.recipe.recipeservice.service.IngredientService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/api/ingredients")
@OpenAPIDefinition(info = @Info(title = "Ingredient Management API", version = "1.0", description = "API to support various Ingredient Operations"))
public class IngredientsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IngredientsController.class);

    private IngredientService ingredientService;

    public IngredientsController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    /***
     *  API Endpoint to add fetch Ingredient by Id
     * @param ingredientId
     * @return boolean
     */
    @Operation(description = "API Endpoint to add fetch Ingredient by Id")
    @GetMapping("/{ingredientId}")
    public ServiceResponseDto<IngredientsDto> fineOne(@PathVariable("ingredientId") Long ingredientId) {
        LOGGER.info("Processing request to a update existing recipe, request : {}", ingredientId);
        return new ServiceResponseDto<>(ingredientService.find(ingredientId));
    }

    /**
     * API Endpoint to add fetch all ingredients
     *
     * @return
     */
    @Operation(description = "API Endpoint to add fetch all ingredients")
    @GetMapping
    public Set<IngredientsDto> findAll() {
        LOGGER.info("Processing request to find all ingredients");
        return ingredientService.findAll();
    }

}
