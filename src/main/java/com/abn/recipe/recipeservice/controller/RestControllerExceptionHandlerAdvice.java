package com.abn.recipe.recipeservice.controller;

import com.abn.recipe.recipeservice.dto.ServiceResponseDto;
import com.abn.recipe.recipeservice.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.UUID;

/**
 * Rest controller Advice to gracefully handle all Exceptions
 */
@ControllerAdvice(annotations = RestController.class)
public class RestControllerExceptionHandlerAdvice extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestControllerExceptionHandlerAdvice.class);

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public ServiceResponseDto handleValidationException(ValidationException validationException) {
        String errorCode = new StringBuilder().append("VE-").append(UUID.randomUUID()).toString();
        String errorMessage = String.format("Validation Failure occurred with the code {}", errorCode);
        LOGGER.error(errorMessage, validationException);
        return new ServiceResponseDto<>(errorCode, validationException.getErrorType().name());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseEntity<String> handleException(Exception exception) {
        String errorCode = new StringBuilder().append("IE-").append(UUID.randomUUID()).toString();
        String errorMessage = String.format("Internal Server Error occurred. Please reach out to the support team with this code {}", errorCode);
        LOGGER.error(errorMessage, exception);
        return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
