package com.abn.recipe.recipeservice.exception;

/**
 * Custom Exception, thrown in case if any invalid input is received
 */
public class ValidationException extends RuntimeException {

    private ErrorType errorType;

    public ValidationException(ErrorType errorType) {
        super(errorType.name());
        this.errorType = errorType;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    @Override
    public String toString() {
        return "ValidationException{" +
                "errorType=" + errorType +
                '}';
    }
}