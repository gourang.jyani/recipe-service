package com.abn.recipe.recipeservice.exception;

/**
 * Enumeration for various Validation Errors being thrown
 */
public enum ErrorType {
    /**
     * Error to indicate invalid parameter in request
     */
    INVALID_REQUEST,
    INVALID_PAGE_REQUEST,
    INVALID_RECIPE_NAME,
    INVALID_RECIPE_INGREDIENTS,
    INVALID_RECIPE_SERVING_QUANTITY,
    INVALID_RECIPE_INSTRUCTIONS;
}
