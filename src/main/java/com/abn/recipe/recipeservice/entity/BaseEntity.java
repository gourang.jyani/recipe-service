package com.abn.recipe.recipeservice.entity;

import java.io.Serializable;

/**
 * Base entity contract, all entities must abide to this contract
 */
public interface BaseEntity extends Serializable {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);


}