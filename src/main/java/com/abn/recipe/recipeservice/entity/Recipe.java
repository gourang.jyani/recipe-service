package com.abn.recipe.recipeservice.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity Class for holding state of Recipes
 */
@Entity
@Table(name = "RECIPE")
public class Recipe implements BaseEntity {

    @Id
    @Column(name = "ID", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "RECIPE_SEQ")
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "IS_VEGETARIAN", nullable = false)
    private boolean vegetarian;

    @Column(name = "SERVING_QUANTITY", nullable = false)
    private Long servingQuantity;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<Ingredients> ingredients = new HashSet<>();

    @Column(name = "INSTRUCTIONS", nullable = false)
    private String instructions;

    public Recipe() {

    }

    public Recipe(Long id, String name, boolean vegetarian, Long servingQuantity, Set<Ingredients> ingredients, String instructions) {
        this.id = id;
        this.name = name;
        this.vegetarian = vegetarian;
        this.servingQuantity = servingQuantity;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public Long getServingQuantity() {
        return servingQuantity;
    }

    public void setServingQuantity(Long servingQuantity) {
        this.servingQuantity = servingQuantity;
    }

    public Set<Ingredients> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredients> ingredients) {
        this.ingredients = ingredients;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Recipe recipe = (Recipe) o;

        return new EqualsBuilder().append(vegetarian, recipe.vegetarian).append(id, recipe.id).append(name, recipe.name).append(servingQuantity, recipe.servingQuantity).append(ingredients, recipe.ingredients).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(name).append(vegetarian).append(servingQuantity).append(ingredients).toHashCode();
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", vegetarian=" + vegetarian +
                ", servingQuantity=" + servingQuantity +
                ", ingredients=" + ingredients +
                ", instructions=" + instructions +
                '}';
    }
}
