package com.abn.recipe.recipeservice.transformer;

import com.abn.recipe.recipeservice.dto.IngredientsDto;
import com.abn.recipe.recipeservice.dto.RecipeDto;
import com.abn.recipe.recipeservice.entity.Ingredients;
import com.abn.recipe.recipeservice.entity.Recipe;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Transformer Implementation to transform DTO to Entity and vice-versa
 */
@Service
public class RecipeTransformerImpl implements RecipeTransformer {
    @Override
    public RecipeDto transformToDto(Recipe source) {
        return RecipeDto.builder()
                .id(source.getId())
                .name(source.getName())
                .vegetarian(source.isVegetarian())
                .servingQuantity(source.getServingQuantity())
                .instructions(source.getInstructions())
                .ingredients(transformToIngredientsDto(source.getIngredients()))
                .build();
    }

    @Override
    public Recipe transformFromDto(RecipeDto source) {
        return new Recipe(source.getId(),
                source.getName(),
                source.isVegetarian(),
                source.getServingQuantity(),
                transformFromIngredientsDto(source.getIngredients()),
                source.getInstructions());
    }

    private Set<IngredientsDto> transformToIngredientsDto(Set<Ingredients> sourceIngredients) {
        return sourceIngredients
                .stream()
                .map(sourceIngredient -> new IngredientsDto(sourceIngredient.getId(), sourceIngredient.getName()))
                .collect(Collectors.toSet());
    }

    private Set<Ingredients> transformFromIngredientsDto(Set<IngredientsDto> sourceIngredients) {
        return sourceIngredients
                .stream()
                .map(sourceIngredient -> new Ingredients(sourceIngredient.getId(), sourceIngredient.getName()))
                .collect(Collectors.toSet());
    }
}
