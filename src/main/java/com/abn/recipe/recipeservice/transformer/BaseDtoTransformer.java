package com.abn.recipe.recipeservice.transformer;

import com.abn.recipe.recipeservice.dto.BaseEntityDto;
import com.abn.recipe.recipeservice.entity.BaseEntity;

/**
 * Blueprint for transformation operations
 *
 * @param <D>
 * @param <E>
 */
public interface BaseDtoTransformer<D extends BaseEntityDto, E extends BaseEntity> {

    D transformToDto(E source);

    E transformFromDto(D source);

}