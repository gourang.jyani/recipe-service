package com.abn.recipe.recipeservice.transformer;

import com.abn.recipe.recipeservice.dto.RecipeDto;
import com.abn.recipe.recipeservice.entity.Recipe;

public interface RecipeTransformer extends BaseDtoTransformer<RecipeDto, Recipe> {

}
