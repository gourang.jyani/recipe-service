package com.abn.recipe.recipeservice.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;

/**
 * Base entity data transfer object
 */
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public abstract class BaseEntityDto implements Serializable {

    private Long id;
    private String name;

    public BaseEntityDto() {
    }
}