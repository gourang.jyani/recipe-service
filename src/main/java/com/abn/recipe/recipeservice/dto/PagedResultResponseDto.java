package com.abn.recipe.recipeservice.dto;

import lombok.Getter;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Set;

/**
 * Standard Dto of providing response in Paginated format.
 *
 * @param <E>
 */
@Getter
public abstract class PagedResultResponseDto<E> implements Serializable {

    protected final Set<E> records;
    protected final Integer totalPages;
    protected final Long totalElements;
    protected final Integer noOfRecords;
    protected final Integer currentPage;

    public PagedResultResponseDto(Set<E> records, Integer totalPages, Long totalElements, Integer currentPage) {
        this.records = records;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
        this.currentPage = currentPage;
        this.noOfRecords = CollectionUtils.isEmpty(records) ? 0 : records.size();
    }
}
