package com.abn.recipe.recipeservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Search Request DTO
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
public class RecipeSearchRequestDto implements Serializable {

    private static final long serialVersionUID = 2563376523116756970L;
    private String name;
    private Boolean vegetarian = null;
    private Long servingQuantity;
    private Set<String> ingredientsIn = new HashSet<>();
    private Set<String> ingredientsNotIn = new HashSet<>();
    private Set<String> instructionKeywords = new HashSet<>();
    private PageRequestDto pageRequest;

    public RecipeSearchRequestDto() {
    }

    public RecipeSearchRequestDto(String name, Boolean vegetarian, Long servingQuantity, Set<String> ingredientsIn, Set<String> ingredientsNotIn, Set<String> instructionKeywords, PageRequestDto pageRequest) {
        this.name = name;
        this.vegetarian = vegetarian;
        this.servingQuantity = servingQuantity;
        this.ingredientsIn = ingredientsIn;
        this.ingredientsNotIn = ingredientsNotIn;
        this.instructionKeywords = instructionKeywords;
        this.pageRequest = pageRequest;
    }
}
