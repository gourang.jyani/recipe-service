package com.abn.recipe.recipeservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Data Transfer Object to hold state of Ingredients record
 */
@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IngredientsDto extends BaseEntityDto {
    
    public IngredientsDto(Long id, String name) {
        super(id, name);
    }
}
