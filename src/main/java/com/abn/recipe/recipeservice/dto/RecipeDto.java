package com.abn.recipe.recipeservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

/**
 * Data Transfer Object to hold state of Recipe record
 */
@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RecipeDto extends BaseEntityDto {

    private boolean vegetarian;
    private Long servingQuantity;
    private Set<IngredientsDto> ingredients;
    private String instructions;

    @Builder
    public RecipeDto(Long id, String name, boolean vegetarian, Long servingQuantity, Set<IngredientsDto> ingredients, String instructions) {
        super(id, name);
        this.vegetarian = vegetarian;
        this.servingQuantity = servingQuantity;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }
}
