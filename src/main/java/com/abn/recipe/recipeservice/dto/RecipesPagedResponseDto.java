package com.abn.recipe.recipeservice.dto;

import java.util.Set;

/**
 * Data Transfer Object for storing the state of Recipe Pagination Response Data
 */
public class RecipesPagedResponseDto extends PagedResultResponseDto<RecipeDto> {

    public RecipesPagedResponseDto(Set<RecipeDto> records, Integer totalPages, Long totalElements, Integer currentPage) {
        super(records, totalPages, totalElements, currentPage);
    }
}
