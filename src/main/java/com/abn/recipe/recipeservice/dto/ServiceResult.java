package com.abn.recipe.recipeservice.dto;

/**
 * Service Result Enum, to indicate Status of Request
 */
public enum ServiceResult {
    SUCCESS, FAILURE;
}