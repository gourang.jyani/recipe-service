package com.abn.recipe.recipeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PageRequestDto {
    private Integer page;
    private Integer pageSize;
}
