package com.abn.recipe.recipeservice.dto;

import java.io.Serializable;
import java.util.UUID;

/**
 * Standard Service Response Wrapper for all endpoint operations
 *
 * @param <T>
 */
public class ServiceResponseDto<T extends Serializable> implements Serializable {

    private T dto;
    private String responseId;
    private ServiceResult serviceResult;
    private String errorMessage;

    public ServiceResponseDto(T dto) {
        this.dto = dto;
        this.responseId = UUID.randomUUID().toString();
        this.serviceResult = ServiceResult.SUCCESS;
    }

    public ServiceResponseDto(String errorMessage) {
        this.dto = null;
        this.responseId = UUID.randomUUID().toString();
        this.serviceResult = ServiceResult.FAILURE;
        this.errorMessage = errorMessage;
    }

    public ServiceResponseDto(String responseId, String errorMessage) {
        this.dto = null;
        this.responseId = responseId;
        this.serviceResult = ServiceResult.FAILURE;
        this.errorMessage = errorMessage;
    }

    public ServiceResponseDto() {
    }

    public T getDto() {
        return dto;
    }

    public void setDto(T dto) {
        this.dto = dto;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public ServiceResult getServiceResult() {
        return serviceResult;
    }

    public void setServiceResult(ServiceResult serviceResult) {
        this.serviceResult = serviceResult;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "ServiceResponseDto{" +
                "dto=" + dto +
                ", responseId='" + responseId + '\'' +
                ", serviceResult=" + serviceResult +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
