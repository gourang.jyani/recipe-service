package com.abn.recipe.recipeservice.service;

import com.abn.recipe.recipeservice.dto.IngredientsDto;

import java.util.Set;

public interface IngredientService {
    Set<IngredientsDto> findAll();

    IngredientsDto find(Long ingredientId);
}
