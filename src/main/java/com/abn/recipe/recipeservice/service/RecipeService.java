package com.abn.recipe.recipeservice.service;

import com.abn.recipe.recipeservice.dto.RecipeDto;
import com.abn.recipe.recipeservice.dto.RecipeSearchRequestDto;
import com.abn.recipe.recipeservice.dto.RecipesPagedResponseDto;
import org.springframework.data.domain.PageRequest;

public interface RecipeService {

    /**
     * Provide implementation for retrieving Recipe from Database
     *
     * @return java.util.Set of all {@link RecipeDto}
     */
    RecipeDto find(Long recipeId);

    /**
     * Provide implementation for adding new Recipe record in database
     *
     * @param recipeDto
     * @return
     */
    boolean add(RecipeDto recipeDto);

    /**
     * Provide implementation for updating an existing Recipe record in database
     *
     * @param recipeDto
     * @return
     */
    boolean update(RecipeDto recipeDto);

    /**
     * Provide implementation for removing a Recipe record from database
     *
     * @param recipeId
     * @return
     */
    boolean delete(Long recipeId);

    /**
     * Provide implementation for fetching all recipe records
     *
     * @param pageRequest
     * @return Set of {@link RecipeDto}
     */
    RecipesPagedResponseDto findAll(PageRequest pageRequest);

    /**
     * Provide implementation for fetching all recipe records matching provided search criteria
     *
     * @param searchRequest
     * @return
     */
    RecipesPagedResponseDto search(RecipeSearchRequestDto searchRequest);


}