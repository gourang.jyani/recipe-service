package com.abn.recipe.recipeservice.service;

import com.abn.recipe.recipeservice.dao.IngredientsRepository;
import com.abn.recipe.recipeservice.dto.IngredientsDto;
import com.abn.recipe.recipeservice.entity.Ingredients;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class IngredientServiceImpl implements IngredientService {

    private IngredientsRepository repository;

    public IngredientServiceImpl(IngredientsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Set<IngredientsDto> findAll() {
        return repository.findAll().stream().map(ingredient -> new IngredientsDto(ingredient.getId(), ingredient.getName())).collect(Collectors.toSet());
    }

    @Override
    public IngredientsDto find(Long ingredientId) {
        Optional<Ingredients> ingredientsOptional = repository.findById(ingredientId);
        return ingredientsOptional.map(ingredient -> new IngredientsDto(ingredient.getId(), ingredient.getName())).orElse(null);

    }
}
