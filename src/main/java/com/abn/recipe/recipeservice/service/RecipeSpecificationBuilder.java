package com.abn.recipe.recipeservice.service;

import com.abn.recipe.recipeservice.dto.RecipeSearchRequestDto;
import com.abn.recipe.recipeservice.entity.Ingredients;
import com.abn.recipe.recipeservice.entity.Recipe;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class RecipeSpecificationBuilder {

    public static Specification<Recipe> buildSpecification(RecipeSearchRequestDto searchRequest) {
        Specification<Recipe> spec = Specification.where(null);

        if (Objects.nonNull(searchRequest.getVegetarian())) {
            spec = spec.and(byIsVegetarian(searchRequest.getVegetarian()));
        }
        if (StringUtils.isNotEmpty(searchRequest.getName())) {
            spec = spec.and(byNameLike(searchRequest.getName()));
        }
        if (Objects.nonNull(searchRequest.getServingQuantity()) && searchRequest.getServingQuantity() > 0) {
            spec = spec.and(byServingQuantity(searchRequest.getServingQuantity()));
        }
        if (!CollectionUtils.isEmpty(searchRequest.getIngredientsIn())) {
            spec = spec.and(byIngredientsIn(searchRequest.getIngredientsIn()));
        }
        if (!CollectionUtils.isEmpty(searchRequest.getIngredientsNotIn())) {
            spec = spec.and(Specification.not(byIngredientsIn(searchRequest.getIngredientsNotIn())));
        }
        if (!CollectionUtils.isEmpty(searchRequest.getInstructionKeywords())) {
            spec = spec.and(byInstructionKeywordsIn(searchRequest.getInstructionKeywords()));
        }
        return spec;
    }

    private static Specification<Recipe> byNameLike(String name) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%");
    }

    private static Specification<Recipe> byIsVegetarian(boolean isVegetarian) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("vegetarian"), isVegetarian);
    }

    private static Specification<Recipe> byServingQuantity(Long servingQuantity) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("servingQuantity"), servingQuantity);
    }

    private static Specification<Recipe> byIngredientsIn(Set<String> ingredients) {
        return (root, query, criteriaBuilder) -> {
            Join<Ingredients, Recipe> ingredientsRecipe = root.join("ingredients");
            return criteriaBuilder.lower(ingredientsRecipe.get("name")).in(ingredients.stream().map(String::toLowerCase).collect(Collectors.toSet()));
        };
    }

    private static Specification<Recipe> byInstructionKeywordsIn(Set<String> instructionKeywords) {
        return (root, query, criteriaBuilder) -> {
            Predicate[] instructionsPredicate = instructionKeywords.stream()
                    .map(instructionKeyword -> criteriaBuilder.like(
                            criteriaBuilder.lower(root.get("instructions")), "%" + instructionKeyword.toLowerCase() + "%"))
                    .toArray(Predicate[]::new);
            return criteriaBuilder.or(instructionsPredicate);
        };
    }
}
