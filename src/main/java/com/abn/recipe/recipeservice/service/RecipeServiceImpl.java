package com.abn.recipe.recipeservice.service;

import com.abn.recipe.recipeservice.dao.RecipeRepository;
import com.abn.recipe.recipeservice.dto.RecipeDto;
import com.abn.recipe.recipeservice.dto.RecipeSearchRequestDto;
import com.abn.recipe.recipeservice.dto.RecipesPagedResponseDto;
import com.abn.recipe.recipeservice.entity.Recipe;
import com.abn.recipe.recipeservice.exception.ErrorType;
import com.abn.recipe.recipeservice.exception.ValidationException;
import com.abn.recipe.recipeservice.transformer.RecipeTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class provides business implementation for all Recipe Operations
 */
@Service
@Transactional
public class RecipeServiceImpl implements RecipeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecipeServiceImpl.class);
    private final RecipeRepository repository;
    private final RecipeTransformer recipeTransformer;
    @Value("${recipe.search.default-page-size}")
    private int DEFAULT_PAGE_SIZE;

    public RecipeServiceImpl(RecipeRepository repository, RecipeTransformer recipeTransformer) {
        this.repository = repository;
        this.recipeTransformer = recipeTransformer;
    }

    @Override
    public RecipeDto find(Long recipeId) {
        Optional<Recipe> optionalRecipe = repository.findById(recipeId);
        return optionalRecipe.map(recipeTransformer::transformToDto).orElse(null);
    }

    @Override
    public boolean add(RecipeDto recipeDto) {
        validate(recipeDto);
        repository.save(recipeTransformer.transformFromDto(recipeDto));
        return Boolean.TRUE;
    }

    @Override
    public boolean update(RecipeDto recipeDto) {

        validate(recipeDto);

        Recipe recipeInDb = repository.findById(recipeDto.getId()).orElse(null);
        if (Objects.isNull(recipeInDb)) {
            throwValidationError(ErrorType.INVALID_REQUEST);
        }

        Recipe updatedRecipe = recipeTransformer.transformFromDto(recipeDto);

        recipeInDb.setName(updatedRecipe.getName());
        recipeInDb.setVegetarian(updatedRecipe.isVegetarian());
        recipeInDb.setServingQuantity(updatedRecipe.getServingQuantity());
        recipeInDb.setIngredients(updatedRecipe.getIngredients());
        recipeInDb.setInstructions(updatedRecipe.getInstructions());

        repository.save(updatedRecipe);

        return Boolean.TRUE;
    }

    @Override
    public boolean delete(Long recipeId) {

        if (Objects.isNull(recipeId)) {
            throwValidationError(ErrorType.INVALID_REQUEST);
        }
        repository.deleteById(recipeId);

        return Boolean.TRUE;
    }

    @Override
    public RecipesPagedResponseDto findAll(PageRequest pageable) {
        final Page<Recipe> recipePage = repository.findAll(pageable);
        return getRecipesPagedResponseDto(recipePage, pageable);
    }

    @Override
    public RecipesPagedResponseDto search(RecipeSearchRequestDto searchRequest) {
        PageRequest pageable;
        if (Objects.isNull(searchRequest.getPageRequest())) {
            pageable = PageRequest.of(0, DEFAULT_PAGE_SIZE);
        } else {
            if (Objects.isNull(searchRequest.getPageRequest().getPage()) || searchRequest.getPageRequest().getPage() < 0) {
                throw new ValidationException(ErrorType.INVALID_PAGE_REQUEST);
            }
            if (Objects.isNull(searchRequest.getPageRequest().getPageSize())) {
                pageable = PageRequest.of(searchRequest.getPageRequest().getPage(), DEFAULT_PAGE_SIZE);
            } else {
                pageable = PageRequest.of(searchRequest.getPageRequest().getPage(), searchRequest.getPageRequest().getPageSize(), Sort.by("name"));
            }
        }

        Page<Recipe> recipePage = repository.findAll(RecipeSpecificationBuilder.buildSpecification(searchRequest), pageable);
        return getRecipesPagedResponseDto(recipePage, pageable);
    }


    private RecipesPagedResponseDto getRecipesPagedResponseDto(Page<Recipe> recipePage, PageRequest pageable) {
        if (CollectionUtils.isEmpty(recipePage.getContent())) {
            LOGGER.error("No content found for current request returning empty response");
            return new RecipesPagedResponseDto(Collections.emptySet(), 0, 0L, 0);
        }

        //Page Index should be greater than 0 and less than Size-1
        if (pageable.getPageNumber() < 0 || pageable.getPageNumber() >= recipePage.getTotalPages()) {
            LOGGER.error("Invalid Page Requested : {}, page should be 0 and TotalPageSize", pageable.getPageNumber());
            throw new ValidationException(ErrorType.INVALID_PAGE_REQUEST);
        }

        final Set<RecipeDto> recipes = recipePage.getContent()
                .stream()
                .map(recipeTransformer::transformToDto)
                .collect(Collectors.toCollection(LinkedHashSet::new));

        return new RecipesPagedResponseDto(recipes, recipePage.getTotalPages(), recipePage.getTotalElements(), recipePage.getNumber() + 1);
    }

    private void validate(RecipeDto recipeDto) {
        if (StringUtils.isEmpty(recipeDto.getName())) {
            throwValidationError(ErrorType.INVALID_RECIPE_NAME);
        } else if (StringUtils.isEmpty(recipeDto.getInstructions())) {
            throwValidationError(ErrorType.INVALID_RECIPE_INSTRUCTIONS);
        } else if (CollectionUtils.isEmpty(recipeDto.getIngredients())) {
            throwValidationError(ErrorType.INVALID_RECIPE_INGREDIENTS);
        } else if (recipeDto.getServingQuantity() <= 0L) {
            throwValidationError(ErrorType.INVALID_RECIPE_SERVING_QUANTITY);
        }
    }

    private void throwValidationError(ErrorType errorType) {
        LOGGER.error("Validation Failed with error : {}", errorType);
        throw new ValidationException(errorType);
    }
}