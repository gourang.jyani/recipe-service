package com.abn.recipe.recipeservice.dao;

import com.abn.recipe.recipeservice.entity.Ingredients;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IngredientsRepository extends JpaRepository<Ingredients, Long> {

}
