## Recipe Service

The recipe Management Service has only one module which offers below functionalities:

1. Add a new recipe
2. Update an existing recipe
3. Delete a recipe by Id
4. Find Recipe based on ID
5. Find All Recipes (Paginated)
6. Search Operation for Recipe (by Name, Vegetarian, Servings, Ingredients, Keywords)

#

## Dependencies

- The application has following external dependencies:
    - Open JDK 11.0
    - H2 Database (In memory)
    - Docker

- Maven Dependencies include the below:
    - spring-boot-starter-web
    - spring-boot-starter-data-jpa
    - h2
    - lombok
    - springdoc-openapi-ui

# How to build this Application?

To build this application run the below command from root directory:
mvn clean install

# Architecture

Recipe Service Application is a Spring Boot Service, which depends on In memory H2 Database This is a dockerized
application.

# How to Run this application

## Using Docker Compose

In order to run this application we need to use docker compose module's functionality (from root directory):

1. In order to build this application use below command:

```
docker-compose build
```

2. In order to run this application in detached mode, use the below command (from root directory):

```
docker-compose up -d
```

3. In order to stop the application, use the below command (from root directory):

```
docker-compose down
```

- Note
    - The application runs on 8080 port

```
sh -c "java -jar recipe-service-1.0.jar"
```

# Open API Documentation

Open API documentation is available at http://localhost:8080/recipe-service/swagger-ui/index.html#/
Spring Doc Open API Documentation is available at http://localhost:8080/recipe-service/api-docs

# Postman Collection

Postman Collection for all requests for this project is available under root directory, which can be readily imported to
Postman

# Java Doc

Java Docs are available under /java-doc directory or /java-docs/index.html

# GIT

https://gitlab.com/gourang.jyani/recipe-service

## API Endpoints:

### Recipe Service APIs

#### Add Recipe

- POST /recipe-service/api/recipe

```
curl --location --request POST 'http://localhost:8080/recipe-service/api/recipe' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Fried Potato",
    "vegetarian": true,
    "servingQuantity": 2,
    "ingredients": [
        {
            "id": 5002,
            "name": "Onion"
        },
        {
            "id": 5001,
            "name": "Potato"
        }
    ],
    "instructions": "Add Potato and Onion mix in Oven for 10 min at temp 220"
}'
```

- Request Body

```
{
    "name": "Fried Potato",
    "vegetarian": true,
    "servingQuantity": 2,
    "ingredients": [
        {
            "id": 5002,
            "name": "Onion"
        },
        {
            "id": 5001,
            "name": "Potato"
        }
    ],
    "instructions": "Add Potato and Onion mix in Oven for 10 min at temp 220"
}
```

- Response Body

```
{
    "dto": true,
    "responseId": "2f581447-6107-46d0-96ae-a7e7aac68cf0",
    "serviceResult": "SUCCESS",
    "errorMessage": null
}
```

#### Update Recipe

- PUT /recipe-service/api/recipe

```
curl --location --request PUT 'http://localhost:8080/recipe-service/api/recipe' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": 6001,
    "name": "Fried Rice",
    "vegetarian": true,
    "servingQuantity": 2,
    "ingredients": [
        {
            "id": 5002,
            "name": "Onion"
        },
        {
            "id": 5005,
            "name": "Fish"
        },
        {
            "id": 5006,
            "name": "Rice"
        }
    ],
    "instructions": "Boil Rice, Add Onion, Chilli, Cook for 10 min"
}'
```

- Request Body

```
{
    "id": 6001,
    "name": "Fried Rice",
    "vegetarian": true,
    "servingQuantity": 2,
    "ingredients": [
        {
            "id": 5002,
            "name": "Onion"
        },
        {
            "id": 5005,
            "name": "Fish"
        },
        {
            "id": 5006,
            "name": "Rice"
        }
    ],
    "instructions": "Boil Rice, Add Onion, Chilli, Cook for 10 min"
}
```

- Response Body

```
{
    "dto": true,
    "responseId": "123f5b78-e42b-44fb-a9ab-d17a9ad8d8b8",
    "serviceResult": "SUCCESS",
    "errorMessage": null
}
```

#### Delete Recipe by Id

- DELETE recipe-service/api/recipe/{recipeId}

```
curl --location --request DELETE 'http://localhost:8080/recipe-service/api/recipe/6001' \
--data-raw ''
```

- Response Body

```
{
    "dto": true,
    "responseId": "501b32e9-00b4-4c58-85a2-254dec30c32c",
    "serviceResult": "SUCCESS",
    "errorMessage": null
}
```

#### Find Recipe By Id

- GET recipe-service/api/recipe/{recipeId}

```
curl --location --request GET 'http://localhost:8080/recipe-service/api/recipe/6002' \
--data-raw ''
```

- Response Body

```
{
    "dto": {
        "id": 6002,
        "name": "Burger",
        "vegetarian": false,
        "servingQuantity": 1,
        "ingredients": [
            {
                "id": 5002,
                "name": "Onion"
            },
            {
                "id": 5004,
                "name": "Chicken"
            },
            {
                "id": 5007,
                "name": "Bread"
            }
        ],
        "instructions": "Take burger buns add ketchup, ham and serve"
    },
    "responseId": "2c3a6fde-8492-493e-bd6f-85b1e68ba432",
    "serviceResult": "SUCCESS",
    "errorMessage": null
}
```

#### Find ALL Recipes (Paginated Response)

- GET /recipe-service/api/recipe?page={requestedPage}&size={pageSize}
- Both the parameters are optional, default page size is 10

```
curl --location --request GET 'http://localhost:8080/recipe-service/api/recipe?page=1&size=2' \
--data-raw ''
```

- Request Params
    - page
        - Optional, integer Value
        - Default Value is First page
    - size
        - Optional, integer Value
        - Default value is 5, can be configured by overriding the property "stock.management-service.page.default.size"
          in application.properties file
- Response Body

```
{
    "dto": {
        "records": [
            {
                "id": 6002,
                "name": "Burger",
                "vegetarian": false,
                "servingQuantity": 1,
                "ingredients": [
                    {
                        "id": 5002,
                        "name": "Onion"
                    },
                    {
                        "id": 5004,
                        "name": "Chicken"
                    },
                    {
                        "id": 5007,
                        "name": "Bread"
                    }
                ],
                "instructions": "Take burger buns add ketchup, ham and serve"
            },
            {
                "id": 6005,
                "name": "Chicken Rice",
                "vegetarian": false,
                "servingQuantity": 2,
                "ingredients": [
                    {
                        "id": 5004,
                        "name": "Chicken"
                    },
                    {
                        "id": 5006,
                        "name": "Rice"
                    }
                ],
                "instructions": "Boil Rice, Add Chicken, Cook for 10 min"
            }
        ],
        "totalPages": 3,
        "totalElements": 5,
        "noOfRecords": 2,
        "currentPage": 1
    },
    "responseId": "581d6984-9276-4ab2-a26b-719dbba6b256",
    "serviceResult": "SUCCESS",
    "errorMessage": null
}
```

#### Search Recipes with filtered request (Paginated Response)

- POST /recipe-service/api/recipe/search
- All params are optional, all recipes are provided if no filters are added in request

```
curl --location --request POST 'http://localhost:8080/recipe-service/api/recipe/search' \
--header 'Content-Type: application/json' \
--data-raw '{
  "name": "rice",
  "vegetarian": true,
  "servingQuantity": null,
  "ingredientsIn": [
  ],
  "ingredientsNotIn": [
      
  ],
  "instructionKeywords": [
  ],
  "pageRequest": {
    "page": 0,
    "pageSize": 10
  }
}'
```

- Response Body

```
{
    "dto": {
        "records": [
            {
                "id": 6004,
                "name": "Lentil Rice",
                "vegetarian": true,
                "servingQuantity": 2,
                "ingredients": [
                    {
                        "id": 5008,
                        "name": "Lentils"
                    },
                    {
                        "id": 5006,
                        "name": "Rice"
                    }
                ],
                "instructions": "Boil Rice, Add Lentils, Cook for 10 min"
            }
        ],
        "totalPages": 1,
        "totalElements": 1,
        "noOfRecords": 1,
        "currentPage": 1
    },
    "responseId": "31c207fc-914e-4e68-b8dd-21a994bccdb3",
    "serviceResult": "SUCCESS",
    "errorMessage": null
}
```
